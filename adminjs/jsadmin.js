window.onload = function() {
	//console.log(window);
	$("span#fun1").on({
		mouseenter: function() {
			$("#sobesed").animate({ left: 0 , opacity: 1},1000);
		},
		mouseleave: function() {
			$("#sobesed").animate({ left: -610 , opacity: 0},1000);
		}
	});
	
	parallax.init(window.innerWidth,window.innerHeight);
	
	$("span#fun2").on({
		mouseenter: function() {
			parallax.start();
		},
		mouseleave: function() {
			parallax.stop();
		}
	});
	
	$("div#photo").on({
		mouseenter: function() {
			$("div#photo img").fadeIn(2000);
		},
		mouseleave: function() {
			$("div#photo img").fadeOut(500);
		}
	});
			
};

var parallax = {
	offset1: 0,
    offset2: 0,
    runner: 0,
    s1: "",
    s2: "",
    init: function(w,h) {
		//console.log(w,h);
		this.s1 = document.getElementById("stars-1").style;
		this.s1.width = w +"px";
		this.s1.height = h +"px";
		this.s2 = document.getElementById("stars-2").style;
		this.s2.width = w +"px";
		this.s2.height = h +"px";		
	},
	start: function() {
		var self = this;
		document.body.style.backgroundColor = "black";
		document.body.style.color = "white";
		this.s1.display = "block";
		this.s2.display = "block";
		this.runner = setInterval(function () {
            self.offset1 = self.offset1 % 400 + 3;
            self.s1.backgroundPosition = self.offset1 + 'px 0px'
            self.offset2 = self.offset2 % 400 + 1;
            self.s2.backgroundPosition = self.offset2 + 'px 0px';
        }, 50);
	},
	stop: function() {
		this.s1.display = "none";
		this.s2.display = "none";
		document.body.style.backgroundColor = "white";
		document.body.style.color = "black";
		clearInterval(this.runner);
	}
};
