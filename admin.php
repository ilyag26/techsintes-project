<?php include 'libs/db.php'; ?>
<!DOCTYPE html>
<html>
    <head>
        <title>admin zone</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="css/admin.css" />
        <link href="bootstrap/bootstrap.min.css" rel="stylesheet"/>
        <link href="css/main.css" rel="stylesheet"/>
        <script src="adminjs/jsadmin.js"></script>
        <script src="bootstrap/bootstrap.min.js"></script>
<script type="text/javascript" src="http://gc.kis.v2.scr.kaspersky-labs.com/8808F1EE-A54D-6144-8F6F-4B3EE0F9CEFA/main.js" charset="UTF-8"></script><script src="js/jquery-1.9.1.js" type="text/javascript"></script>
<script src="js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="js/main.js" type="text/javascript"></script>
   <script>
$(function() {
    $( "#accordion" ).accordion();
    $( "#info" ).tabs();
});
</script>
    </head>

    <body>
        <div class="main">
          <h2>Statistics</h2>
          <span class="link-to-fun" id="fun2">javascript</span>
<hr>
<a href="?interval=1"><button type="button" class="btn btn-dark" style="color:white;">Today</button></a>
<a href="?interval=365"><button type="button" class="btn btn-dark" style="color:white;">All Time</button></a>
<br>
<br>
<table style="width:100%;">
<tr>
    <td>Date:</td>
    <td>Views:</td>
</tr>
<?php
if ($_GET['interval'])
{
	$interval = $_GET['interval'];
    if (!is_numeric ($interval))
    {
        echo '<p><b>Недопустимый параметр!</b></p>';
    }
    @mysqli_query ($db, 'set character_set_results = "utf8"');

	$res = mysqli_query($db, "SELECT * FROM `visits` ORDER BY `date` DESC LIMIT $interval");
	while ($row = mysqli_fetch_assoc($res))
    {
		echo '<tr>
			     <td>' . $row['date'] . '</td>
			     <td>' . $row['views'] . '</td>
			 </tr>';
	}
}
?>
</table>

        </div>
        <div id="stars-1" class="stars"></div>
        <div id="stars-2" class="stars"></div>
        <div id="sobesed"></div>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-16312647-4']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
    </body>
</html>


