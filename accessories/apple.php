<?php include '../libs/db.php'; ?>
<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>IQOS 3 Multi</title>
    <link href="../bootstrap/bootstrap.min.css" rel="stylesheet"/>
    <link href="../css/main.css" rel="stylesheet"/>
    <script src="../js/jquery-3.2.1.min.js"></script>
    <script src="../bootstrap/js.js"></script>
    <script src="../bootstrap/bootstrap.min.js"></script>
    <link rel="shortcut icon" href="../images/logotechsintesback.png" type="image/png">

  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

  <!-- Optional theme -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
</head>
<body>

    <div id="header" style="font-size:35px;">
IQOS 3 Multi
    </div>

     
 <div class="main-panel">
       <div class="row">
  <div class="col-md-2">      
      <a href="../index.php" class="buttons mainmenubtn" > <img src="../images/home.png" width=70 height=70></a> 
  </div>
  <div class="col-md-8">
    <div class="dropdown1">       
  <a class="link3 mainmenubtn" onmouseover="this.style.color='black';" onmouseout="this.style.color='white';" style="font-size:50px;">&nbsp;&nbsp;&equiv;&nbsp;&nbsp;</a>
  <div class="dropdown-child">
  
  <div class="dropdown">
  <m data-toggle="dropdown" ><a href="#"  class="link2">Iphones</a></m>
  <ul class="dropdown-menu">
    <li class="dropdown-item disabled"><a>New Iphones</a></li>
       <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;нет в наличии</li>
    <li class="dropdown-item disabled"><a>refurbished IPhones</a></li>
    <li><a href="../iphone/iphoneXN.html">Iphone X refurbished</a></li>
    <li><a href="../iphone/iphone8N.html">Iphone 8 refurbished</a></li>
    <li><a href="../iphone/iphone8plusN.html">Iphone 8 plus refurbished</a></li>
    <li><a href="../iphone/iphone7N.html">Iphone 7 refurbished</a></li>
    <li><a href="../iphone/iphone7plusN.html">Iphone 7 plus refurbished</a></li>
   </ul>
  </div><br>
 <div class="dropdown">
    <m data-toggle="dropdown"><a href="#"  class="link2">Iqos</a></m>
    <ul class="dropdown-menu">
  <li><a href="../IQOS2.4Plus.html">IQOS 2.4 Plus</a></li>
  <li><a href="../IQOS3.html">IQOS 3</a></li>
  <li><a href="../IQOS3Multi.html">IQOS 3 Multi</a></li>
    </ul>
  </div>
  <br>
   <div class="dropdown">
    <m data-toggle="dropdown"><a href="#"  class="link2">Аксессуары</a></m>
    <ul class="dropdown-menu">
    <li><a href="apple.html?interval=365">apple</a></li>
    </ul>
  </div>
    <hr>
    <a href="../q.php" class="buttons"><img src="../images/q.png" width=30 height=40></a>
  </div>
</div>
  </div>

</div>


</div>
    
    
    <br>
         <br>
<br>
<br>
<div class="panels-shop">
 
  <div class="shop-rub"><?php
if ($_GET['interval'])
{
  $interval = $_GET['interval'];
    if (!is_numeric ($interval))
    {
        echo '<p><b>Недопустимый параметр!</b></p>';
    }
    @mysqli_query ($db, 'set character_set_results = "utf8"');

  $res = mysqli_query($db, "SELECT * FROM `product` ORDER BY `kod` DESC LIMIT $interval");
  while ($row = mysqli_fetch_assoc($res))
    {
    echo $row['price'] . 'грн';
  }
}
?></div>
   <div class="shop-new">Номер товара:<?php
if ($_GET['interval'])
{
  $interval = $_GET['interval'];
    if (!is_numeric ($interval))
    {
        echo '<p><b>Недопустимый параметр!</b></p>';
    }
    @mysqli_query ($db, 'set character_set_results = "utf8"');

  $res = mysqli_query($db, "SELECT * FROM `product` ORDER BY `kod` DESC LIMIT $interval");
  while ($row = mysqli_fetch_assoc($res))
    {
    echo $row['kod'];
  }
}?></div>
  <div  class="image2"><?php
if ($_GET['interval'])
{
  $interval = $_GET['interval'];
    if (!is_numeric ($interval))
    {
        echo '<p><b>Недопустимый параметр!</b></p>';
    }
    @mysqli_query ($db, 'set character_set_results = "utf8"');

  $res = mysqli_query($db, "SELECT * FROM `product` ORDER BY `kod` DESC LIMIT $interval");
  while ($row = mysqli_fetch_assoc($res))
    {
    echo $row['image'];
  }
}?></div><br><br>
  <div class="fonts"><?php
if ($_GET['interval'])
{
  $interval = $_GET['interval'];
    if (!is_numeric ($interval))
    {
        echo '<p><b>Недопустимый параметр!</b></p>';
    }
    @mysqli_query ($db, 'set character_set_results = "utf8"');

  $res = mysqli_query($db, "SELECT * FROM `product` ORDER BY `kod` DESC LIMIT $interval");
  while ($row = mysqli_fetch_assoc($res))
    {
    echo $row['name'];
  }
}?></div>
  <a href="../form.php" class="buttons link-main link">Заказать</a>    
</div>
<br>
         <br>
     <!-- jQuery -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <!-- Latest compiled and minified JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  <div class="footer">

          &copy; techsintes<a href="https://www.instagram.com/techsintes/"> <img src="../images/instagram.png" width="37" height="30"></a>
      </div>           

</body>
</html>
